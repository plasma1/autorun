from telegram import Bot

#Create simple class from it


class TelegramBot:
    def __init__(self):
        self.bot = Bot(self.getApiKey())
        self.chatId = self.getChatId()

    @staticmethod
    def getApiKey():
        with open("./private/botToken") as file:
            ApiKey = file.read().rstrip("\n")
            return ApiKey

    @staticmethod
    def getChatId():
        with open("./private/chatId") as file:
            chatId = file.read().rstrip("\n")
            return chatId

    def sendStartMessage(self, name):
        self.bot.send_message(chat_id = self.chatId,
                         text = "Program {} - started.".format(name))

    def sendGoodMessage(self, name):
        self.bot.send_message(chat_id = self.chatId,
                         text = "Program {} - completed.".format(name))
    
    def sendInterruptMessage(self, name):
        self.bot.send_message(chat_id = self.chatId,
                         text = "Program {} - interrupted.".format(name))

if (__name__ == "__main__"):
    bot = TelegramBot()
    bot.sendGoodMessage("pe_0140")