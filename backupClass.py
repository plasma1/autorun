from botClass import TelegramBot
import os, shutil

#Backup and restore with remove unfinished

class BackupProcess:
    def __init__(self, dirPath):
        self.picRuns = "{dirPath}/PlasmaEnlightenment/runs/".format(dirPath = dirPath)
        self.bot = TelegramBot()

    def checkJson(self,parameters):
        error = None
        listArray = [len(value) for value in parameters.values() if (type(value) is list)]
        if (len(set(listArray)) > 1):
            error = ValueError("json arrays has different length")
        dictArray = [int(max(value, key=lambda x: int(x))) for value in parameters.values() if (type(value) is dict)]
        srtArrayLen = len([value for value in parameters.values() if (type(value) is str)])
        srtArrayIndex = 1 if (srtArrayLen != 0) else 0
        dictArrayIndex = 0 if (len(dictArray) == 0) else max(dictArray)#Get max index
        listArrayIndex = 0 if (len(listArray) == 0) else listArray[0]

        if (dictArrayIndex > 1) and (listArrayIndex == 0):
            error = ValueError("json dict has too big index")
        if (srtArrayIndex == 0) and (dictArrayIndex == 0) and (listArrayIndex == 0):
            error = ValueError("no experiments to run")
        if error is not None:
            shutil.rmtree(self.dirPath)
            self.bot.sendInterruptMessage(self.dirName)
            raise error
        return max(listArrayIndex, dictArrayIndex, srtArrayIndex)

    def startIndex(self,dirName):
        self.dirName = dirName
        self.bot.sendStartMessage(self.dirName)
        self.dirPath = self.picRuns+self.dirName+"/"
        self.backupFilePath = self.dirPath+".backup"
        if (os.path.exists(self.dirPath) == False):
            os.mkdir(self.dirPath)
        if (os.path.exists(self.backupFilePath)):
            with open(self.backupFilePath) as backupFile:
                indexString = backupFile.read()
                return int(indexString)
        else:
            return 0

    def setIndex(self, index):
        self.index = str(index+1)
        with open(self.backupFilePath,"w") as backupFile:
            backupFile.write(str(index))


    def remove(self):
        #Программа прервана, написать что вы прервали выполнение, удалить последнюю папку
        try:
            shutil.rmtree(self.dirPath+self.index)
        except FileNotFoundError:
            pass
        self.bot.sendInterruptMessage(self.dirName)

    def clean(self):
        #Программа удачно завершена, удалить .backup
        os.remove(self.backupFilePath)
        self.bot.sendGoodMessage(self.dirName)
    
    def log(self):
        pass