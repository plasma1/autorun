import subprocess, signal, os
from time import sleep

from editClass import FileEditor
from backupClass import BackupProcess
#Runs plasma in numbered subdirs
#Check if POpen exited with non zero code? and then send break message

class Experiment:
    def __init__(self):
        self.interrupted = False#do we need it?
        self.rootPath = os.path.realpath(os.path.join(os.path.dirname(__file__),"../../"))
        self.picInputs = os.path.join(self.rootPath,"PlasmaEnlightenment/picInputs/")
        self.fileEditor = FileEditor(self.picInputs)
        self.backup = BackupProcess(self.rootPath)
        self.listenInterrupt = False
        self.command = (". $SPACK\n"
                        "spack load picongpu\n"
                        "spack load cmake\n"
                        "pic-build\n"
                        "tbg -s bash -c etc/picongpu/1.cfg -t etc/picongpu/bash/mpiexec.tpl {rootPath}/PlasmaEnlightenment/runs/{dirName}/{subDirNumber}\n"
                        "python3 {rootPath}/PostProcessing/VideoGeneration/main.py {rootPath}/PlasmaEnlightenment/runs/{dirName}/{subDirNumber}\
                        -i mpl -v pngUnified -b 10M -c hevc_nvenc -f 20")

    def run(self, dirName, subDirNumber):
        def terminate_process(signum, frame):
            if (self.interrupted == False) and (self.listenInterrupt == True):
                os.killpg(os.getpgid(plasmaProcess.pid), signal.SIGTERM)
                print("Interrupting program...")
                sleep(1)
                self.interrupted = True

        commandFormated = self.command.format(rootPath = self.rootPath, dirName=dirName, subDirNumber = subDirNumber)
        plasmaProcess = subprocess.Popen(commandFormated, cwd=self.picInputs, shell=True, 
            universal_newlines=True, start_new_session=True)
        #Press Ctrl+C to terminate execution
        signal.signal(signal.SIGINT, terminate_process)
        self.listenInterrupt = True
        returnCode = plasmaProcess.wait()
        self.listenInterrupt = False
        return returnCode


    def runSeries(self, dirName, parameters):
        startIndex = self.backup.startIndex(dirName)
        endIndex = self.backup.checkJson(parameters)
        #Define from where to start
        for index in range(startIndex, endIndex):
            self.backup.setIndex(index)#backup this position
            subDirNumber = index + 1
            self.fileEditor.editAll(parameters, index)
            self.run(dirName,subDirNumber = subDirNumber)
            if (self.interrupted ==True):
                self.backup.remove()
                break
        else:
            self.backup.clean()





if (__name__ == "__main__"):

    valueArray = ["0.6e25","0.8e25","1.0e25"]
    parameter = "BASE_DENSITY_SI"

    experiment = Experiment()


    #experiment.runSeries("pe_0140", parameter, valueArray)
