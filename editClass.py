import json, re

#Edit file to run then
#Now create class

class FileEditor:
    def __init__(self, picInputsPath):
        with open("./private/edit.json") as editJsonFile:
            self.editJson = json.loads(editJsonFile.read())
        self.picInputsPath = picInputsPath

    @staticmethod
    def extractParameter(value, index):
        #Vulnerability, cycle does not fail due to difficult conditions
        if (type(value) is list) and (value[index] != "//"):
            return value[index]
        elif (type(value) is dict) and (str(index+1) in value):
            return value[str(index+1)]
        elif (type(value) is str) and (index == 0):
            return value
        return None

    def edit(self, parameter, setValue):
        searchFilePath = self.picInputsPath+self.editJson[parameter]["file"]
        with open(searchFilePath) as searchFile:
            searchString = searchFile.read()
        searchPattern = self.editJson[parameter]["pattern"]
        #editing File. \\ - to use * in regeX
        modifiedString = re.sub(re.escape(searchPattern).replace("//\\*\\*//", ".+"),
                                searchPattern.replace("//**//", setValue),searchString)
        with open(searchFilePath,"w") as editFile:
            editFile.write(modifiedString)

    def editAll(self,parameters,index):
        for parameter, valueArray in parameters.items():
            value = self.extractParameter(valueArray, index)
            if (value is not None):
                self.edit(parameter, value)
        print("Set all parameters")



if (__name__ == "__main__"):
    #Creating path to dir
    import os
    dirPath = os.path.realpath(os.path.join(os.path.dirname(__file__),"../../"))
    picInputsPath = os.path.join(dirPath,"PlasmaEnlightenment/picInputs/")
    #Editing file
    parameter = "BASE_DENSITY_SI"
    setValue = "2.3e25"
    fileEditor = FileEditor(picInputsPath)
    #To to avoid random changes
    #fileEditor.edit(parameter, setValue)
