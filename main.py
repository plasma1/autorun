import json, os, shutil
from runClass import Experiment


#Read json and start simulation
#Copy json to run dir

def readAndCopyParameters(picRuns):
    jsonFilePath = "./parameters.json"
    with open(jsonFilePath) as jsonFile:
        parametersJson = json.loads(jsonFile.read())
    #copy parameters to data dir
    dirPath = picRuns+parametersJson['projectName']+"/"
    if (os.path.exists(dirPath) == False):
        os.mkdir(dirPath)
    #If dir already completed, then stop init
    elif (os.path.exists(dirPath) == True) and (os.path.exists(dirPath+".backup") == False):
        raise ValueError("The directory {} already used for another project".format(parametersJson['projectName']))

    shutil.copyfile(jsonFilePath,dirPath+jsonFilePath[2:])
    #get parameters
    projectName = parametersJson["projectName"]
    parameters = parametersJson["parameters"]
    return projectName, parameters



experiment = Experiment()
projectName, parameters = readAndCopyParameters(experiment.backup.picRuns)

experiment.runSeries(projectName, parameters)
